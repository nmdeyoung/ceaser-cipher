# Ceaser Cipher

## Build

Run `go build ceaser_cipher.go`.

## Runtime

Note: Defaults are already loaded into the script. Without flags, the program will encrypt "THISrISrArTEST" with a shift pattern of 4.

Flags:
    `-m` flag can either be "encrypt" or "decrypt". "encrypt is default.
    `-t` flag is the text to encrypt/decrypt. "THISrISrArTEST" is default.
    `-s` flag is the shift pattern. 4 is the default.

## Input Example

`./ceaser_cipher -m encrypt -t THISrISrArTEST -s 4`

## Output Example

```
Text provided:  THISrISrArTEST
Shift pattern:  4
Encrypted message:  XLMWvMWvEvXIWX
```

or

```
Text provided:  XLMWvMWvEvXIWX
Shift pattern:  4
Decrypted message:  THISrISrArTEST
```
