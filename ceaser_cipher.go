package main

import (
	"bytes"
	"flag"
	"fmt"
	"unicode"
)

func encrypt(text string, shift int) (message string){
	var e_message bytes.Buffer
	for _, t := range text {
		if unicode.IsUpper(t) == true {
			m := string((int(t) + shift - 65) % 26 + 65)
			e_message.WriteString(m)
		} else {
			m := string((int(t) + shift - 97) % 26 + 97)
			e_message.WriteString(m)
		}
	}
	return e_message.String()
}

func decrypt(text string, shift int) (message string){
	var d_message bytes.Buffer
	for _, t := range text {
		if unicode.IsUpper(t) == true {
			m := string((int(t) - shift - 65) % 26 + 65)
			d_message.WriteString(m)
		} else {
			m := string((int(t) - shift - 97) % 26 + 97)
			d_message.WriteString(m)
		}
	}
	return d_message.String()
}

func main() {
	// Get CLI flags.
	var method = flag.String("m", "encrypt", "Encrypt or decrypt provided text.")
	var text = flag.String("t", "THISrISrArTEST", "Text to encrypt/decrypt")
	var shift = flag.Int("s", 4, "Shift pattern")
	flag.Parse()

	fmt.Println("Text provided: ", *text)
	fmt.Println("Shift pattern: ", *shift)

	// Encrypting or decrypting text.
	switch {
	case *method == "encrypt":
		message := encrypt(*text, *shift)
		fmt.Println("Encrypted message: ", message)
	case *method == "decrypt":
		message := decrypt(*text, *shift)
		fmt.Println("Decrypted message: ", message)
	default:
		fmt.Println("Unknown method provided.")
	}
}

